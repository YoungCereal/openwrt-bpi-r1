#!/bin/bash
# Debug script uncomment set -x
#set -x
cpucount=$(grep -c "processor" /proc/cpuinfo)
cpu=$(($cpucount + 1))

if [ "$1" = "multicore" ] ; then
        IGNORE_ERRORS=m,y make -j $cpu V=s 2>&1| tee errors.txt
        [ -f build_*.txt ] && rm build_*.txt
    else
        IGNORE_ERRORS=m,y make V=s 2>&1| tee errors.txt
        [ -f build_*.txt ] && rm build_*.txt
fi

for i in $(grep "failed to build" errors.txt | sed 's/^.*ERROR:[[:space:]]*\([^[:space:]].*\) failed to build.*$/\1/' ) ; do
        if [ "$i" != "" ] ; then
                echo Compiling: ${i}
                make ${i}-compile V=s > build_${i##*/}.txt 2>&1 || echo ${i} : Build failed, see build_${i##*/}.txt
        fi
done
