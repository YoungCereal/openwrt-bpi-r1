#
# Copyright (C) 2013 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.

define KernelPackage/rtc-sunxi
    SUBMENU:=$(OTHER_MENU)
    TITLE:=Sunxi SoC built-in RTC support
    DEPENDS:=@TARGET_sunxi
    $(call AddDepends/rtc)
    KCONFIG:= \
	CONFIG_RTC_CLASS=y \
	CONFIG_RTC_DRV_SUNXI=m
    FILES:=$(LINUX_DIR)/drivers/rtc/rtc-sunxi.ko
    AUTOLOAD:=$(call AutoLoad,50,rtc-sunxi)
endef

define KernelPackage/rtc-sunxi/description
 Support for the AllWinner sunXi SoC's onboard RTC
endef

$(eval $(call KernelPackage,rtc-sunxi))

define KernelPackage/sunxi-ir
    SUBMENU:=$(OTHER_MENU)
    TITLE:=Sunxi SoC built-in IR support (A20)
    DEPENDS:=@TARGET_sunxi +kmod-input-core
    $(call AddDepends/rtc)
    KCONFIG:= \
	CONFIG_MEDIA_SUPPORT=y \
	CONFIG_MEDIA_RC_SUPPORT=y \
	CONFIG_RC_DEVICES=y \
	CONFIG_IR_SUNXI
    FILES:=$(LINUX_DIR)/drivers/media/rc/sunxi-cir.ko
    AUTOLOAD:=$(call AutoLoad,50,sunxi-cir)
endef

define KernelPackage/sunxi-ir/description
 Support for the AllWinner sunXi SoC's onboard IR (A20)
endef

$(eval $(call KernelPackage,sunxi-ir))

define KernelPackage/eeprom-sunxi
    SUBMENU:=$(OTHER_MENU)
    TITLE:=AllWinner Security ID fuse support
    DEPENDS:=@TARGET_sunxi
    KCONFIG:= \
	CONFIG_EEPROM_SUNXI_SID
    FILES:=$(LINUX_DIR)/drivers/misc/eeprom/sunxi_sid.ko
    AUTOLOAD:=$(call AutoLoad,50,sunxi_sid)
endef

define KernelPackage/eeprom-sunxi/description
 Support for the AllWinner Security ID fuse support
endef

$(eval $(call KernelPackage,eeprom-sunxi))

define KernelPackage/ata-sunxi
    TITLE:=AllWinner sunXi AHCI SATA support
    SUBMENU:=$(BLOCK_MENU)
    DEPENDS:=@TARGET_sunxi +kmod-ata-ahci-platform +kmod-scsi-core
    KCONFIG:=CONFIG_AHCI_SUNXI
    FILES:=$(LINUX_DIR)/drivers/ata/ahci_sunxi.ko
    AUTOLOAD:=$(call AutoLoad,41,ahci_sunxi,1)
endef

define KernelPackage/ata-sunxi/description
 SATA support for the AllWinner sunXi SoC's onboard AHCI SATA
endef

$(eval $(call KernelPackage,ata-sunxi))

define KernelPackage/sun4i-emac
  SUBMENU:=$(NETWORK_DEVICES_MENU)
  TITLE:=AllWinner EMAC Ethernet support
  DEPENDS:=@TARGET_sunxi
  KCONFIG:=CONFIG_SUN4I_EMAC
  FILES:=$(LINUX_DIR)/drivers/net/ethernet/allwinner/sun4i-emac.ko
  AUTOLOAD:=$(call AutoProbe,sun4i-emac)
endef

$(eval $(call KernelPackage,sun4i-emac))


define KernelPackage/wdt-sunxi
    SUBMENU:=$(OTHER_MENU)
    TITLE:=AllWinner sunXi Watchdog timer
    DEPENDS:=@TARGET_sunxi
    KCONFIG:=CONFIG_SUNXI_WATCHDOG
    FILES:=$(LINUX_DIR)/drivers/$(WATCHDOG_DIR)/sunxi_wdt.ko
    AUTOLOAD:=$(call AutoLoad,51,sunxi_wdt)
endef

define KernelPackage/wdt-sunxi/description
    Kernel module for AllWinner sunXi watchdog timer.
endef

$(eval $(call KernelPackage,wdt-sunxi))


define KernelPackage/sound-soc-sunxi
  TITLE:=AllWinner built-in SoC sound support
  KCONFIG:= \
	CONFIG_SND_SUNXI_SOC_CODEC
  FILES:= \
	$(LINUX_DIR)/sound/soc/sunxi/sunxi-codec.ko
  AUTOLOAD:=$(call AutoLoad,65,sunxi-codec)
  DEPENDS:=@TARGET_sunxi +kmod-sound-soc-core @LINUX_4_1
  $(call AddDepends/sound)
endef

define KernelPackage/sound-soc-sunxi/description
  Kernel support for AllWinner built-in SoC audio
endef

$(eval $(call KernelPackage,sound-soc-sunxi))

define KernelPackage/hwmon-axp20x
  SUBMENU:=$(HWMON_MENU)
  TITLE:=AXP20X Sensor Support
  DEPENDS:=@TARGET_sunxi
  $(call AddDepends/hwmon)
  KCONFIG:= \
	CONFIG_MFD_AXP20X=y \
	CONFIG_SENSORS_AXP20X=m
  FILES:=$(LINUX_DIR)/drivers/hwmon/axp20x-hwmon.ko
  AUTOLOAD:=$(call AutoLoad,51,axp20x-hwmon)
  
endef

define KernelPackage/hwmon-axp20x/description
 Kernel module for AXP20X thermal monitor chip
endef

$(eval $(call KernelPackage,hwmon-axp20x))

define KernelPackage/net-stmmac
  SUBMENU:=$(NETWORK_DEVICES_MENU)
  TITLE:=STMicroelectronics 10/100/1000 Ethernet Support
  DEPENDS:=+kmod-mii
  KCONFIG:=CONFIG_STMMAC_ETH=m \
    CONFIG_STMMAC_PLATFORM=m
  FILES:= \
	$(LINUX_DIR)/drivers/net/ethernet/stmicro/stmmac/stmmac.ko \
	$(LINUX_DIR)/drivers/net/ethernet/stmicro/stmmac/stmmac-platform.ko
  AUTOLOAD:=$(call AutoLoad,43,stmmac stmmac-platform,1)
endef

define KernelPackage/net-stmmac/description
 Kernel module for STMicroelectronics 10/100/1000 Ethernet Support
endef

$(eval $(call KernelPackage,net-stmmac))
