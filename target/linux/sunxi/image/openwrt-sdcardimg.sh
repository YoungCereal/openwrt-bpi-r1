#!/bin/bash
#set -x
#env > envlist.txt

if (( EUID != 0 )); then
    echo "You must run the script with sudo or root" 1>&2
    exit 1
fi

version=
basedir=
rootfactory=
rootsysupgrade=
bootfiles=
uboot=
bootsize=
rootfactorysize=
rootsysupgradesize=

mkdir -p ${basedir}
cd ${basedir}


# Create the disk and partition it
dd if=/dev/zero of=${basedir}/$version-factory.img bs=1M count=$rootfactorysize
parted $version-factory.img --script -- mklabel msdos
parted $version-factory.img --script -- mkpart primary fat32 2048s "$bootsize"M
parted $version-factory.img --script -- mkpart primary ext4 "$bootsize"M 100%

sleep 3

# Set the partition variables with packages
loopdevice0=`losetup -f --show ${basedir}/$version-factory.img`
device0=`kpartx -va $loopdevice0| sed -E 's/.*(loop[0-9]*)p.*/\1/g' | head -1`
sleep 5
device0="/dev/mapper/${device0}"
bootpimg0=${device0}p1
rootpimg0=${device0}p2

# Create file systems
mkfs.vfat $bootpimg0
mkfs.ext4 $rootpimg0

# Create the dirs for the partitions and mount them
mkdir -p ${basedir}/bootdest ${basedir}/rootdest ${basedir}/rootsrc
mount $bootpimg0 ${basedir}/bootdest
mount $rootpimg0 ${basedir}/rootdest

echo "Mounting root factory ext4 image then rsyncing to root loop img"
mount $rootfactory ${basedir}/rootsrc
rsync -HPavz -q ${basedir}/rootsrc/ ${basedir}/rootdest/

echo "Copy boot files to boot partition"
cp $bootfiles/* ${basedir}/bootdest

echo "Inject u-boot image to loop img"
dd if=${basedir}/bootdest/$uboot of=$loopdevice0 bs=1024 seek=8

sleep 5

# Unmount partitions
umount $bootpimg0
umount $rootpimg0
umount ${basedir}/rootsrc

kpartx -dv $loopdevice0

sleep 5

# Create the disk and partition it
dd if=/dev/zero of=${basedir}/$version-sysupgrade.img bs=1M count=$rootsysupgradesize
parted $version-sysupgrade.img --script -- mklabel msdos
parted $version-sysupgrade.img --script -- mkpart primary fat32 2048s "$bootsize"M
parted $version-sysupgrade.img --script -- mkpart primary ext4 "$bootsize"M 100%

sleep 3

# Set the partition variables with packages
loopdevice1=`losetup -f --show ${basedir}/$version-sysupgrade.img`
device1=`kpartx -va $loopdevice1| sed -E 's/.*(loop[0-9]*)p.*/\1/g' | head -1`
sleep 5
device1="/dev/mapper/${device1}"
bootpimg1=${device1}p1
rootpimg1=${device1}p2

# Create file systems
mkfs.vfat $bootpimg1
mkfs.ext4 $rootpimg1

# Create the dirs for the partitions and mount them
mount $bootpimg1 ${basedir}/bootdest
mount $rootpimg1 ${basedir}/rootdest

echo "Mounting root sysupgrade ext4 image then rsyncing to root loop img"
mount $rootsysupgrade ${basedir}/rootsrc
rsync -HPavz -q ${basedir}/rootsrc/ ${basedir}/rootdest/ --exclude=packages

echo "Copy boot files to boot partition"
cp $bootfiles/* ${basedir}/bootdest

echo "Inject u-boot image to loop img"
dd if=${basedir}/bootdest/$uboot of=$loopdevice1 bs=1024 seek=8

sleep 5

# Unmount partitions
umount $bootpimg1
umount $rootpimg1
umount ${basedir}/rootsrc

kpartx -dv $loopdevice1

sleep 5

echo "Cleanup Src/Destination directories"
rm -rf ${basedir}/rootsrc ${basedir}/rootdest ${basedir}/bootdest

echo "Cleanup Loop devices.."
losetup -d $loopdevice0
losetup -d $loopdevice1

echo "Change permissions ${basedir}/ back to current user.."
chown -R $SUDO_USER ${basedir}

echo "Generating sha1sum of $version-factory.img"
sha1sum $version-factory.img > ${basedir}/$version-factory.img.sha1sum

echo "Generating sha1sum of $version-sysupgrade.img"
sha1sum $version-sysupgrade.img > ${basedir}/$version-sysupgrade.img.sha1sum

exit 0

